package subsequence;

import java.util.ArrayList;
import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        // TODO: Implement the logic here
        if(x == null || y == null){
            throw new IllegalArgumentException();
        }
        List<String> list1 = new ArrayList<>();
        List<String> list2 = new ArrayList<>();
        List<String> tempList1 = new ArrayList<>();

        for (Object o : x) {
            list1.add(String.valueOf(o));
        }
        for (Object o : y) {
            list2.add(String.valueOf(o));
        }
        tempList1.addAll(list1);

        if(!list2.containsAll(list1)) return false;

        for(int i = 0; i < list2.size(); i++){
            if(!list1.contains(list2.get(i))){
                list2.remove(i);
                i-=1;
            }else {
                list1.remove(list2.get(i));
            }
        }

        return list2.equals(tempList1);
    }

    }

