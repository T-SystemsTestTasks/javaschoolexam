package calculator;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

public class Calculator {
    static Deque<Node> countQueue = new LinkedList<>();
    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public String evaluate(String statement) {
        // TODO: Implement the logic here
        try {
            if(statement == null) return null;
            if(statement.contains(",")) return null;


            List<String> statementAsList = splitStatement(statement);

            for(int i = 0; i < statementAsList.size(); i++){
                if(statementAsList.get(i).contains("(")){
                    String temp = (String)statementAsList.get(i).subSequence(statementAsList.get(i).indexOf("(")+1, statementAsList.get(i).lastIndexOf(")"));
                    String a = solve(splitStatement(temp));
                    statementAsList.remove(i);
                    statementAsList.add(i, a);
                }
            }
            String result = solve(statementAsList);

            String[] resultAsArray = result.split("");
            boolean isDouble = false;

            for(int i = 0; i < resultAsArray.length; i++){
                if(resultAsArray[i].equals(".")){
                    for(int j = i+1; j < resultAsArray.length; j++){
                        if(!resultAsArray[j].equals("0")){
                            isDouble=true;
                        }
                    }
                }
            }

            if(!isDouble){
                return (String)result.subSequence(0, result.indexOf("."));
            }

            return result;
        }catch (Exception e){
            return null;
        }


    }

    static double plus (double a, double b){
        return (double) (a + b);
    }
    static double minus (double a, double b){
        return (double) (a - b);
    }
    static double multiply(double a, double b){
        return (double)(a*b);
    }
    static double division(double a, double b){
        return (double)(a/b);
    }

    private static void fillTheQueue(Node node){

        if(node.leftChild != null){
            if(!node.checked){
                countQueue.addFirst(node);
                node.checked=true;
            }
            fillTheQueue(node.leftChild);
            fillTheQueue(node.rightChild);
        }
    }

    private static void refreshChecked(Node node){
        node.checked = false;
        if(node.leftChild != null){
            refreshChecked(node.leftChild);
        }
        if(node.rightChild != null){
            refreshChecked(node.rightChild);
        }
    }

    private static List<String> splitStatement(String statement){

        String[] statementAsArray = statement.split("");
        List<String> statementAsList = new ArrayList<>();
        StringBuilder member = new StringBuilder();

        for(int i = 0; i < statementAsArray.length; i++){
            if(!statementAsArray[i].equals("+") && !statementAsArray[i].equals("-") && !statementAsArray[i].equals("*") && !statementAsArray[i].equals("/")){
                member.append(statementAsArray[i]);
            }else {
                statementAsList.add(member.toString());
                statementAsList.add(statementAsArray[i]);
                member = new StringBuilder();
            }

            if(statementAsArray[i].equals("(")){
                member = new StringBuilder();
                int j = i;
                while (!statementAsArray[j].equals(")")){
                    member.append(statementAsArray[j]);
                    j++;
                }
                member.append(")");
                i = j;
            }
        }

        statementAsList.add(member.toString());


        for(int i = 0; i < statementAsList.size(); i++){
            if(statementAsList.get(i).equals("")){
                statementAsList.remove(i);
            }
        }

        for(int i = 0; i < statementAsList.size(); i++){
            if(statementAsList.get(i).equals("")){
                statementAsList.remove(i);
            }
        }
        return statementAsList;
    }

    private static String solve(List<String> statementAsList){
        List<Node> tree = new ArrayList<>();
        for(int i = 0; i < statementAsList.size(); i++){
            tree.add(new Node(statementAsList.get(i)));
        }


        //создаем ноды для * и /
        for(int i = 0; i < tree.size(); i++){
            if((tree.get(i).root.equals("*") || tree.get(i).root.equals("/")) && !tree.get(i).checked){

                Node node = new Node(tree.get(i).root);
                node.leftChild = tree.get(i-1);
                node.rightChild = tree.get(i+1);
                node.leftChild.parent = node;
                node.rightChild.parent = node;

                tree.remove(i);
                tree.remove(i);
                tree.remove(i-1);
                tree.add(i-1, node);
                node.checked = true;
                i=0;
            }

        }

        for(int i = 0; i < tree.size(); i++){
            tree.get(i).checked = false;
        }

        // создаем ноды для - и +

        for(int i = 0; i < tree.size(); i++){
            if((tree.get(i).root.equals("+") || tree.get(i).root.equals("-")) && !tree.get(i).checked){
                Node node = new Node(tree.get(i).root);
                node.leftChild = tree.get(i-1);
                node.rightChild = tree.get(i+1);
                node.leftChild.parent = node;
                node.rightChild.parent = node;

                tree.remove(i);
                tree.remove(i);
                tree.remove(i-1);
                tree.add(i-1, node);
                i = 0;
            }
        }


        Node x = tree.get(0);
        refreshChecked(x);
        fillTheQueue(x);


        for(Node z : countQueue){
            if(z.root.equals("*")){
                double a = Double.parseDouble(z.leftChild.root);
                double b = Double.parseDouble(z.rightChild.root);
                z.root = Double.toString(multiply(a, b));
            }

            if(z.root.equals("/")){
                double a = Double.parseDouble(z.leftChild.root);
                double b = Double.parseDouble(z.rightChild.root);

                z.root = Double.toString(division(a, b));

            }

            if(z.root.equals("+")){
                double a = Double.parseDouble(z.leftChild.root);
                double b = Double.parseDouble(z.rightChild.root);
                z.root = Double.toString(plus(a, b));
            }

            if(z.root.equals("-")){
                double a = Double.parseDouble(z.leftChild.root);
                double b = Double.parseDouble(z.rightChild.root);
                z.root = Double.toString(minus(a, b));
            }

        }

        String result = countQueue.getLast().root;
        countQueue = new LinkedList<>();
        return result;
    }
}
class Node{
    boolean checked = false;

    String root; //  + - * /

    Node parent = null;
    Node leftChild = null;
    Node rightChild = null;

    public Node(String root) {
        this.root = root;
    }

}