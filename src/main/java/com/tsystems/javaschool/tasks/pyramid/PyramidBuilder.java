package pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        if(inputNumbers.size()>30){
            throw new com.tsystems.javaschool.tasks.pyramid.CannotBuildPyramidException();
        }

        for(int i = 0; i < inputNumbers.size(); i++){
            if(inputNumbers.get(i)==null){
                throw new com.tsystems.javaschool.tasks.pyramid.CannotBuildPyramidException();
            }
        }

        List<Integer> list = new ArrayList<>(inputNumbers);
        Collections.sort(list);
        List<List<Integer>> rows = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            int j = i + 1;
            if (j <= list.size()) {
                List<Integer> tlist = new ArrayList<>();
                for (int z = 0; z < j; z++) {
                    tlist.add(list.get(0));
                    list.remove(0);
                }
                rows.add(tlist);
            }
        }

        int countOfRows = 1;

        for (int i = 0; i < rows.size() - 1; i++) {
            countOfRows += 2;
        }
        int center = countOfRows / 2;

        int[][] result = new int[rows.size()][countOfRows];


        for (int i = 0; i < result.length; i++) {
            for (int j = 0; j < result[i].length; j++) {
                result[i][j] = 0;
            }
        }

        for (int i = 0; i < rows.size(); i++) {

            List<Integer> timeList = rows.get(i);
            int row = timeList.size() - 1; //0  1   2
            if (row == 0) {
                result[row][center] = timeList.get(i);
            } else {

                int firstNum = center - row;
                result[row][firstNum] = timeList.get(0);

                for (int j = 0; j < timeList.size() - 1; j++) {
                    firstNum += 2;
                    result[row][firstNum] = timeList.get(j + 1);
                }
            }
        }
        return result;

    }
}